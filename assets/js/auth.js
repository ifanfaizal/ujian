var form, loadingWrapper, error, success;

document.addEventListener('DOMContentLoaded', function () {
    form = document.getElementById("form_login_regis");
    error = document.getElementsByClassName('error')[0];
    success = document.getElementsByClassName('success')[0];
    loadingWrapper = document.getElementById('loading-wrapper');

    loadingWrapper.style.visibility = 'hidden';
    error.style.visibility = 'hidden';
    success.style.visibility = 'hidden';

    var locationWindow = window.location;
    if (locationWindow.hash == '#register') {
        var tab2 = document.getElementById('tab-2');
        tab2.click();
    }
});

function showError() {
    error.style.visibility = 'visible';
    setTimeout(function(){ hideError(); }, 3000);
}

function hideError() {
    error.style.visibility = 'hidden';
}

function showSuccess() {
    success.style.visibility = 'visible';
}

function hideSuccess() {
    success.style.visibility = 'hidden';
}

function showLoader() {
    loadingWrapper.style.visibility = 'visible';
}

function hideLoader() {
    loadingWrapper.style.visibility = 'hidden';
}

function login() {
    showLoader();

    var username = document.getElementById('user_login');
    var pass = document.getElementById('pass_login');

    if (pass.value == '' || username.value == '') {
        if(pass.value == '')
            pass.classList.add('error');
        else
            pass.classList.remove('error');

        if(username.value == '')
            username.classList.add('error');
        else
            username.classList.remove('error');

        alert('Isi semua field terlebih dahulu!');
        return;
    }

    var elements = document.getElementsByClassName("formVal");
    var formData = new FormData(); 
    var object = {};
    for(var i=0; i<elements.length; i++)
    {
        formData.append(elements[i].name, elements[i].value);
    }
    formData.forEach(function(value, key){
        object[key] = value;
    });

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("post", "../../../ujian/controllers/user/check_user.php"); 
    xmlHttp.setRequestHeader('Content-Type', 'application/json');
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState === 4) {
            if (xmlHttp.status !== 200) {
                var errorRes = JSON.parse(xmlHttp.responseText);
                var innerText = document.getElementById('error-text');
                innerText.innerHTML = errorRes.message;
                showError();
            }

            var user = JSON.parse(xmlHttp.responseText);

            if (user.level == 1) {
                window.location = './admin_page.php';
            } else if (user.level == 2) {
                window.location = './user_page.php';
            }

            hideLoader();
        }
    }
    xmlHttp.send(JSON.stringify(object));
}

function register() {
    showLoader();

    var pass = document.getElementById('pass_regis');
    var confPass = document.getElementById('pass_conf_regis');
    var email = document.getElementById('email_regis');
    var username = document.getElementById('user_regis');

    if (pass.value == '' || confPass.value == '' || email.value == '' || username.value == '') {
        if(pass.value == '')
            pass.classList.add('error');
        else
            pass.classList.remove('error');

        if(confPass.value == '')
            confPass.classList.add('error');
        else
            confPass.classList.remove('error');

        if(email.value == '')
            email.classList.add('error');
        else
            email.classList.remove('error');

        if(username.value == '')
            username.classList.add('error');
        else
            username.classList.remove('error');

        alert('Isi semua field terlebih dahulu!');
        return;
    }

    if (!validateEmail(email.value)) {
        email.classList.add('error');
        alert('Email tidak valid!');
        return;
    } else {
        email.classList.remove('error');
    }

    if (pass.value != confPass.value) {
        pass.classList.add('error');
        confPass.classList.add('error');
        alert('Password konfirmasi tidak sama, silakan ulangi!');
        return;
    } else {
        pass.classList.remove('error');
        confPass.classList.remove('error');
    }

    var elements = document.getElementsByClassName("formValRegis");
    var formData = new FormData(); 
    var object = {};
    for(var i=0; i<elements.length; i++)
    {
        formData.append(elements[i].name, elements[i].value);
    }
    formData.forEach(function(value, key){
        object[key] = value;
    });

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("post", "../../../ujian/controllers/user/create.php"); 
    xmlHttp.setRequestHeader('Content-Type', 'application/json');
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState === 4) {
            if (xmlHttp.status === 201) {
                var successRes = JSON.parse(xmlHttp.responseText);
                var innerText = document.getElementById('success-text');
                innerText.innerHTML = successRes.message;
                showSuccess();
                hideLoader();
            } else if (xmlHttp.status !== 200) {
                var errorRes = JSON.parse(xmlHttp.responseText);
                var innerText = document.getElementById('error-text');
                innerText.innerHTML = errorRes.message;
                showError();
                hideLoader();
            }
        }
    }
    xmlHttp.send(JSON.stringify(object));
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}