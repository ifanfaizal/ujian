var loadingWrapper, pickupLat, pickupLng, destinationLat, destinationLng;

document.addEventListener('DOMContentLoaded', function () {
    var menu1, menu2, menu3, menu4, menu5;
    menu1 = document.getElementById('menu-1');
    menu2 = document.getElementById('menu-2');
    menu3 = document.getElementById('menu-3');
    menu4 = document.getElementById('menu-4');
    // menu5 = document.getElementById('menu-5');
    loadingWrapper = document.getElementById('loading-wrapper');

    var locationWindow = window.location;
    if (locationWindow.pathname === '/ujian/views/user_page.php') {
        showLoader();

        menu1.setAttribute('class', 'pure-menu-selected');
        menu2.setAttribute('class', '');
        menu3.setAttribute('class', '');
        menu4.setAttribute('class', '');
        // menu5.setAttribute('class', '');

        var table = document.getElementById('cucian-table');
        var tbody = document.createElement('tbody');

        var xmlHttpReadAll = new XMLHttpRequest();
        xmlHttpReadAll.open("get", "../../../ujian/controllers/transaction/read_all.php"); 
        xmlHttpReadAll.onreadystatechange = function()
        {
            if(xmlHttpReadAll.readyState === 4) {
                if (xmlHttpReadAll.status === 200) {
                    var transactions = JSON.parse(xmlHttpReadAll.responseText);
                    
                    if (transactions.length > 0) {
                        transactions.forEach(element => {
                            // table row creation
                            var row = document.createElement("tr");
    
                            // Form and set the inner HTML directly
                            row.innerHTML = 
                                `<td class="text-center">${element.cat_name}</td>
                                <td class="text-center">${element.weight} Kg</td>
                                <td class="text-center">${convertToRupiah(element.total_price)}</td>
                                <td class="text-center">${element.mobile_phone}</td>
                                <td class="text-center">${element.created_at}</td>
                                <td class="text-center">
                                    <span class="${element.status == 0 ? 'buble-error' : 
                                                    element.status == 1 ? 'buble-warning' : 
                                                        element.status == 2 ? 'buble-secondary' : 
                                                            'buble-success' } button-small">
                                        ${element.status_ket}
                                    </span>
                                </td>`;
    
                            //row added to end of table body
                            tbody.appendChild(row);
                        });
    
                    } else {
                        var row = document.createElement("tr");
    
                        // Form and set the inner HTML directly
                        row.innerHTML = 
                            `<td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>`;

                        //row added to end of table body
                        tbody.appendChild(row);
                    }

                    table.appendChild(tbody);
                }

                hideLoader();
            }
        }
        xmlHttpReadAll.send();

        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("get", "../../../ujian/controllers/transaction/read_dashboard.php?status=0"); 
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    var data = JSON.parse(xmlHttp.responseText);
                    document.getElementById('active-today-transaction').innerHTML = data.todayCount;
                    document.getElementById('active-month-transaction').innerHTML = `${data.monthCount} <span>bulan ini</span>`;
                    document.getElementById('active-total-transaction').innerHTML = data.totalCount;
                }

                hideLoader();
            }
        }
        xmlHttp.send();

        var xmlHttpFinish = new XMLHttpRequest();
        xmlHttpFinish.open("get", "../../../ujian/controllers/transaction/read_dashboard.php?status=3"); 
        xmlHttpFinish.onreadystatechange = function()
        {
            if(xmlHttpFinish.readyState === 4) {
                var data = JSON.parse(xmlHttpFinish.responseText);
                document.getElementById('finish-today-transaction').innerHTML = data.todayCount;
                document.getElementById('finish-month-transaction').innerHTML = `${data.monthCount} <span>bulan ini</span>`;
                document.getElementById('finish-total-transaction').innerHTML = data.totalCount;

                hideLoader();
            }
        }
        xmlHttpFinish.send();
    } else if (locationWindow.pathname === '/ujian/views/admin_page.php') {
        showLoader();

        menu1.setAttribute('class', 'pure-menu-selected');
        menu2.setAttribute('class', '');
        menu4.setAttribute('class', '');
        // menu5.setAttribute('class', '');

        getAllCucianData();

        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("get", "../../../ujian/controllers/transaction/read_dashboard_admin.php?status=0"); 
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    var data = JSON.parse(xmlHttp.responseText);
                    document.getElementById('active-today-transaction').innerHTML = data.todayCount;
                    document.getElementById('active-month-transaction').innerHTML = `${data.monthCount} <span>bulan ini</span>`;
                    document.getElementById('active-total-transaction').innerHTML = data.totalCount;
                }

                hideLoader();
            }
        }
        xmlHttp.send();

        var xmlHttpFinish = new XMLHttpRequest();
        xmlHttpFinish.open("get", "../../../ujian/controllers/transaction/read_dashboard_admin.php?status=3"); 
        xmlHttpFinish.onreadystatechange = function()
        {
            if(xmlHttpFinish.readyState === 4) {
                var data = JSON.parse(xmlHttpFinish.responseText);
                document.getElementById('finish-today-transaction').innerHTML = data.todayCount;
                document.getElementById('finish-month-transaction').innerHTML = `${data.monthCount} <span>bulan ini</span>`;
                document.getElementById('finish-total-transaction').innerHTML = data.totalCount;

                hideLoader();
            }
        }
        xmlHttpFinish.send();
    } else if (locationWindow.pathname === '/ujian/views/price_page.php') {
        showLoader();

        menu1.setAttribute('class', '');
        menu2.setAttribute('class', 'pure-menu-selected');
        menu3.setAttribute('class', '');
        menu4.setAttribute('class', '');
        // menu5.setAttribute('class', '');

        var table = document.getElementById('price-table');
        var tbody = document.createElement('tbody');

        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("get", "../../../ujian/controllers/price/read.php"); 
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState === 4) {
                if (xmlHttp.status !== 200) {
                    var errorRes = JSON.parse(xmlHttp.responseText);
                    alert(errorRes.message);
                    hideLoader();
                }

                var price = JSON.parse(xmlHttp.responseText);
                price.forEach(element => {
                    // table row creation
                    var row = document.createElement("tr");

                    // Form and set the inner HTML directly
                    row.innerHTML = 
                        `<td class="text-center">${element.category_name}</td>
                        <td class="text-center">${convertToRupiah(element.price)}</td>`;

                    //row added to end of table body
                    tbody.appendChild(row);
                });

                table.appendChild(tbody);
                hideLoader();
            }
        }
        xmlHttp.send();
    } else if (locationWindow.pathname === '/ujian/views/form_page.php') {
        showLoader();

        menu1.setAttribute('class', '');
        menu2.setAttribute('class', '');
        menu3.setAttribute('class', 'pure-menu-selected');
        menu4.setAttribute('class', '');
        // menu5.setAttribute('class', '');

        var noHp = document.getElementById('hp');
        var alamatJemput = document.getElementById('alamat_jemput');
        var alamatAntar = document.getElementById('alamat_antar');
        var selectCategory = document.getElementById('kategori');
        var btnSubmit = document.getElementById('submitForm');

        setInputFilter(document.getElementById("hp"), function(value) {
            return /^\d*\.?\d*$/.test(value);
        });

        btnSubmit.addEventListener("click",function(e){
            if (noHp.value == '') {
                alert('Anda harus mengisi nomor HP!');
            } else if (alamatJemput.value == '') {
                alert('Anda harus memilih alamat penjemputan cucian!');
            } else if (alamatAntar.value == '') {
                alert('Anda harus memilih alamat pengantaran cucian!');
            } else if (selectCategory.value == 'pilih') {
                selectCategory.setAttribute('style', 'background: #ff000047');
                alert('Anda harus memilih kategori cucian!');
            } else {
                var elements = document.getElementsByClassName("formVal");
                var formData = new FormData(); 
                var object = {};
                for(var i=0; i<elements.length; i++)
                {
                    formData.append(elements[i].name, elements[i].value);
                }
                formData.forEach(function(value, key){
                    object[key] = value;
                });

                var xmlHttpCreate = new XMLHttpRequest();
                xmlHttpCreate.open("post", "../../../ujian/controllers/transaction/create.php"); 
                xmlHttpCreate.setRequestHeader('Content-Type', 'application/json');
                xmlHttpCreate.onreadystatechange = function()
                {
                    if(xmlHttpCreate.readyState === 4) {
                        if (xmlHttpCreate.status !== 200) {
                            var errorRes = JSON.parse(xmlHttpCreate.responseText);
                        }

                        var data = JSON.parse(xmlHttpCreate.responseText);
                        console.log(data);

                        hideLoader();

                        window.location = './user_page.php';
                    }
                }
                xmlHttpCreate.send(JSON.stringify(object));
            }
        },false);

        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("get", "../../../ujian/controllers/category/read.php"); 
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState === 4) {
                if (xmlHttp.status !== 200) {
                    var errorRes = JSON.parse(xmlHttp.responseText);
                    alert(errorRes.message);
                    hideLoader();
                }

                var price = JSON.parse(xmlHttp.responseText);
                price.forEach(element => {
                    var option = document.createElement('option');
                    option.innerHTML = `${element.category_name} (${element.category_price}/Kg)`;
                    option.setAttribute('value', element.id);
                    option.setAttribute('data', element.category_price);
                    selectCategory.appendChild(option);
                });

                hideLoader();
            }
        }
        xmlHttp.send();
    } else if (locationWindow.pathname === '/ujian/views/under_maintenance.php') {
        showLoader();

        menu1.setAttribute('class', '');
        menu2.setAttribute('class', '');
        if (menu3 != null)
            menu3.setAttribute('class', '');
        menu4.setAttribute('class', 'pure-menu-selected');

        hideLoader();
    } else if (locationWindow.pathname === '/ujian/views/price_page_admin.php') {
        showLoader();

        menu1.setAttribute('class', '');
        menu2.setAttribute('class', 'pure-menu-selected');
        menu4.setAttribute('class', '');
        // menu5.setAttribute('class', '');

        setInputFilter(document.getElementById("price"), function(value) {
            return /^\d*\.?\d*$/.test(value);
        });

        var btnSubmit = document.getElementById('submitForm');
        var categoryName = document.getElementById('category_name');
        var price = document.getElementById('price');

        btnSubmit.addEventListener("click",function(e){
            if (categoryName.value == '') {
                alert('Anda harus mengisi nama kategori!');
            } else if (price.value == '') {
                alert('Anda harus memilih harga kategori!');
            } else {
                showLoader();
                
                var elements = document.getElementsByClassName("formVal");
                var formData = new FormData(); 
                var object = {};
                for(var i=0; i<elements.length; i++)
                {
                    formData.append(elements[i].name, elements[i].value);
                }
                formData.forEach(function(value, key){
                    object[key] = value;
                });

                var xmlHttpCreate = new XMLHttpRequest();
                xmlHttpCreate.open("post", "../../../ujian/controllers/category/create.php"); 
                xmlHttpCreate.setRequestHeader('Content-Type', 'application/json');
                xmlHttpCreate.onreadystatechange = function()
                {
                    if(xmlHttpCreate.readyState === 4) {
                        if (xmlHttpCreate.status !== 200) {
                            var errorRes = JSON.parse(xmlHttpCreate.responseText);
                        }

                        var data = JSON.parse(xmlHttpCreate.responseText);
                        console.log(data);
                        getAllPrice();
                        categoryName.value = "";
                        price.value = "";
                        hideLoader();
                    }
                }
                xmlHttpCreate.send(JSON.stringify(object));
            }
        },false);

        getAllPrice();
    } else {
        // Kalo url ga jelas (biar loading ga muncul)
        hideLoader();
    }
});

function hapusKategoriDanHarga(idKategori) {
    var xmlHttpChangeStatus = new XMLHttpRequest();
    xmlHttpChangeStatus.open("get", `../../../ujian/controllers/category/delete.php?id=${idKategori}`); 
    xmlHttpChangeStatus.onreadystatechange = function()
    {
        if(xmlHttpChangeStatus.readyState === 4) {
            var data = JSON.parse(xmlHttpChangeStatus.responseText);
            console.log(data);
            getAllPrice();
            hideLoader();
        }
    }
    xmlHttpChangeStatus.send();
}

function getAllPrice() {
    var tbodyWillRemoved = document.querySelector( '#tbodyPrice' );

    if (tbodyWillRemoved != null)
        tbodyWillRemoved.remove();

    var table = document.getElementById('price-table');
    var tbody = document.createElement('tbody');
    tbody.setAttribute('id', 'tbodyPrice');

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("get", "../../../ujian/controllers/price/read.php"); 
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState === 4) {
            if (xmlHttp.status !== 200) {
                var errorRes = JSON.parse(xmlHttp.responseText);
                alert(errorRes.message);
                hideLoader();
            }

            var price = JSON.parse(xmlHttp.responseText);
            price.forEach(element => {
                // table row creation
                var row = document.createElement("tr");

                // Form and set the inner HTML directly
                row.innerHTML = 
                    `<td class="text-center">${element.category_name}</td>
                    <td class="text-center">${convertToRupiah(element.price)}</td>
                    <td class="text-center">
                        <button class="pure-button button-xsmall button-error text-white" onclick="hapusKategoriDanHarga(${element.category_id});">Hapus</button>
                    </td>`;

                //row added to end of table body
                tbody.appendChild(row);
            });

            table.appendChild(tbody);
            hideLoader();
        }
    }
    xmlHttp.send();
}

function getAllCucianData() {
    var tbodyWillRemoved = document.querySelector( '#tbodyCucian' );

    if (tbodyWillRemoved != null)
        tbodyWillRemoved.remove();

    var table = document.getElementById('cucian-table');
    var tbody = document.createElement('tbody');
    tbody.setAttribute('id', 'tbodyCucian');

    var xmlHttpReadAll = new XMLHttpRequest();
    xmlHttpReadAll.open("get", "../../../ujian/controllers/transaction/read_all_admin.php"); 
    xmlHttpReadAll.onreadystatechange = function()
    {
        if(xmlHttpReadAll.readyState === 4) {
            if (xmlHttpReadAll.status === 200) {
                var transactions = JSON.parse(xmlHttpReadAll.responseText);
                
                if (transactions.length > 0) {
                    transactions.forEach(element => {
                        // table row creation
                        var row = document.createElement("tr");
                        row.setAttribute('id', 'row-' + element.id);

                        // Form and set the inner HTML directly
                        row.innerHTML = 
                            `<td class="text-center">
                                <a style="cursor: pointer;" onclick="getMapTransaction(this)" data-id-transaction="${element.id}">${element.cust_uname}</a>
                            </td>
                            <td class="text-center hidden pickup_lat">${element.pickup_point_lat}</td>
                            <td class="text-center hidden pickup_lng">${element.pickup_point_lng}</td>
                            <td class="text-center hidden destination_lat">${element.destination_point_lat}</td>
                            <td class="text-center hidden destination_lng">${element.destination_point_lng}</td>
                            <td class="text-center">${element.cat_name}</td>
                            <td class="text-center">${element.weight} Kg</td>
                            <td class="text-center">${convertToRupiah(element.total_price)}</td>
                            <td class="text-center">${element.mobile_phone}</td>
                            <td class="text-center">
                                <span class="${element.status == 0 ? 'buble-error' : 
                                                element.status == 1 ? 'buble-warning' : 
                                                    element.status == 2 ? 'buble-secondary' : 
                                                        'buble-success' } button-small">
                                    ${element.status_ket}
                                </span>
                            </td>
                            <td>
                                <select onchange="selectAksi(this)" id="pilih-aksi-${element.id}">
                                    <option value="pilih">Pilih</option>
                                    <option value="0" data="${element.id}">Dijemput</option>
                                    <option value="1" data="${element.id}">Dicuci</option>
                                    <option value="2" data="${element.id}">Diantar</option>
                                    <option value="3" data="${element.id}">Selesai</option>
                                </select>
                            </td>`;

                        //row added to end of table body
                        tbody.appendChild(row);
                    });

                } else {
                    var row = document.createElement("tr");

                    // Form and set the inner HTML directly
                    row.innerHTML = 
                        `<td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>`;

                    //row added to end of table body
                    tbody.appendChild(row);
                }

                table.appendChild(tbody);
            }

            hideLoader();
        }
    }
    xmlHttpReadAll.send();
}

function getMapTransaction(obj) {
    var idTransaction = obj.getAttribute('data-id-transaction');
    usernameSelected = obj.innerHTML;
    pickupLat = document.querySelector(`#row-${idTransaction} .pickup_lat`).innerHTML;
    pickupLng = document.querySelector(`#row-${idTransaction} .pickup_lng`).innerHTML;
    destinationLat = document.querySelector(`#row-${idTransaction} .destination_lat`).innerHTML;
    destinationLng = document.querySelector(`#row-${idTransaction} .destination_lng`).innerHTML;
    getLocation();
    document.getElementById('modalMap').style.display='block';
}

function selectAksi(obj) {
    if (obj.value != 'pilih') {
        obj.setAttribute('style', 'background: #fff');
        var status = obj.options[obj.selectedIndex].getAttribute('value');
        var idTransaksi = obj.options[obj.selectedIndex].getAttribute('data');

        var xmlHttpChangeStatus = new XMLHttpRequest();
        xmlHttpChangeStatus.open("get", `../../../ujian/controllers/transaction/change_status_transaction.php?id=${idTransaksi}&status=${status}`); 
        xmlHttpChangeStatus.onreadystatechange = function()
        {
            if(xmlHttpChangeStatus.readyState === 4) {
                var data = JSON.parse(xmlHttpChangeStatus.responseText);
                console.log(data);
                getAllCucianData();
                document.getElementById
                hideLoader();
            }
        }
        xmlHttpChangeStatus.send();
    }
}

function getKategoriSelected(obj) {
    if (obj.value != 'pilih') {
        obj.setAttribute('style', 'background: #fff');
        var price = obj.options[obj.selectedIndex].getAttribute('data');

        document.getElementById('cat_id').value = obj.value;
        document.getElementById('cat_price').value = price;
    }
}

function showLoader() {
    loadingWrapper.style.visibility = 'visible';
}

function hideLoader() {
    loadingWrapper.style.visibility = 'hidden';
}

function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('') + ',-';
}
 
function convertToAngka(rupiah)
{
	return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}

// Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    });
  }