var overlay, btnNav;

document.addEventListener('DOMContentLoaded', function () {
    overlay = document.getElementById("overlay_div");
    btnNav = document.getElementById('btn_nav');

    btnNav.addEventListener('click', function (event) {
        unfade(overlay);
    });

    overlay.addEventListener('click', function (event) {
        fade(overlay);
    });
});

function toggleClass(ele, class1) {
    var classes = ele.className;
    var regex = new RegExp('\\b' + class1 + '\\b');
    var hasOne = classes.match(regex);
    class1 = class1.replace(/\s+/g, '');
    if (hasOne)
      ele.className = classes.replace(regex, '');
    else
      ele.className = classes + class1;
  }

function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 10);
}

function unfade(element) {
    var op = 0.1;  // initial opacity
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}