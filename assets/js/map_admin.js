var dataLaundry = [{
    "code": "0001",
    "lat": "1.28210155945393",
    "lng": "103.81722480263163",
    "location": "Stop 1"
}, {
    "code": "0003",
    "lat": "1.2777380589964",
    "lng": "103.83749709165197",
    "location": "Stop 2"
}, {
    "code": "0002",
    "lat": "1.27832046633393",
    "lng": "103.83762574759974",
    "location": "Stop 3"
}];

function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      console.log("Geolocation is not supported by this browser.");
    }
}

function onMapClick(popup, mymap, e) {
    var eventLatLng = e.latlng;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("get", `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${e.latlng.lat}&lon=${e.latlng.lng}`); 
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState === 4) {
            if (xmlHttp.status === 200) {
                var data = JSON.parse(xmlHttp.responseText);
                popup
                    .setLatLng(eventLatLng)
                    .setContent(`
                        <b>Hallo, ${username}</b><br>
                        Sekarang Anda sedang berada di <br><br>
                        <b>${data.display_name}</b>`
                    ).openOn(mymap);

                document.getElementById('alamatJemputPopup').onclick = function(e) {
                    document.getElementById('alamat_jemput').value = data.display_name;
                    document.getElementById('alamat_jemput_lat').value = eventLatLng.lat;
                    document.getElementById('alamat_jemput_lng').value = eventLatLng.lng;
                }

                document.getElementById('alamatAntarPopup').onclick = function(e) {
                    document.getElementById('alamat_antar').value = data.display_name;
                    document.getElementById('alamat_antar_lat').value = eventLatLng.lat;
                    document.getElementById('alamat_antar_lng').value = eventLatLng.lng;
                }
            }
        }
    }
    xmlHttp.send();
}

function showPosition(position) {
    var mymap = L.map('mapidAdmin').setView([pickupLat, pickupLng], 13);

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("get", `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${pickupLat}&lon=${pickupLng}`); 
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState === 4) {
            if (xmlHttp.status === 200) {
                var data = JSON.parse(xmlHttp.responseText);
                if (pickupLat != destinationLat) {
                    var markerPickup = L.marker([pickupLat,pickupLng]).addTo(mymap);
                    markerPickup.bindPopup(`
                        <b>Cucian untuk pengguna bernama ${usernameSelected}</b><br>
                        Ini adalah lokasi penjemputan cucian <br><br>
                        <b>${data.display_name}</b>`
                    ).openPopup();

                    var markerDest = L.marker([destinationLat,destinationLng]).addTo(mymap);
                    markerDest.bindPopup(`
                        <b>Cucian untuk pengguna bernama ${usernameSelected}</b><br>
                        Ini adalah lokasi pengantaran cucian <br><br>
                        <b>${data.display_name}</b>`
                    ).openPopup();
                } else {
                    var marker = L.marker([pickupLat,pickupLng]).addTo(mymap);
                    marker.bindPopup(`
                        <b>Cucian untuk pengguna bernama ${usernameSelected}</b><br>
                        Ini adalah lokasi penjemputan dan pengantaran cucian <br><br>
                        <b>${data.display_name}</b>`
                    ).openPopup();
                }
            }
        }
    }
    xmlHttp.send();

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaWZhbmZhaXphbCIsImEiOiJjanB3Nmk4dXcwemQ5NDJxbGQ3NHRjNDRnIn0.fMJ0dxrpPer0XAVI-WRHZA', {
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);
    
    // for (var i=0; i<5; i++) {
    //     var randomnumber = Math.random() * (10 - 1 + 1) + 1;
    //     var marker1 = L.marker([currLat + 0.1, currLng + 0.2]).addTo(mymap);
    // }

    var popup = L.popup();
    mymap.on('click', function(e) {
        onMapClick(popup, mymap, e);
    });
}

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
}