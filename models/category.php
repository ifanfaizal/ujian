<?php
class Category{
 
    // database connection and table name
    private $conn;
    private $table_name = "category";
 
    // object properties
    public $id;
    public $category_name;
    public $price;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read categories
    public function read(){
 
        // select all query
        $query = "SELECT
                    c.id, c.category_name, p.price
                FROM
                    " . $this->table_name . " c
                INNER JOIN
                    pricing p
                        ON c.id = p.category_id;";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create category
    function create(){
    
        try {
            // query to insert record
            $queryInsertCategory = "INSERT INTO
                " . $this->table_name . "
                SET
                    category_name=:category_name;";

            $queryInsertPricing = "INSERT INTO
                    pricing
                SET
                    category_id=:category_id, price=:price;";

            // prepare query
            $stmtInsertCategory = $this->conn->prepare($queryInsertCategory);
            $stmtInsertPricing = $this->conn->prepare($queryInsertPricing);

            // sanitize
            $this->category_name=htmlspecialchars(strip_tags($this->category_name));
            $this->price=htmlspecialchars(strip_tags($this->price));

            // bind values
            $stmtInsertCategory->bindParam(":category_name", $this->category_name);

            // execute query
            if($stmtInsertCategory->execute()){
                $lastId = $this->conn->lastInsertId();
                $stmtInsertPricing->bindParam(":category_id", $lastId);
                $stmtInsertPricing->bindParam(":price", $this->price);

                if($stmtInsertPricing->execute()){
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
        
    }

    // delete the product
    function delete(){
    
        // delete query
        $queryStatus = "DELETE FROM pricing WHERE category_id = ?";
        $queryDeleteCategory = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmtStatus = $this->conn->prepare($queryStatus);
        $stmtDeleteCategory = $this->conn->prepare($queryDeleteCategory);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmtStatus->bindParam(1, $this->id);
        $stmtDeleteCategory->bindParam(1, $this->id);
    
        // execute query
        if($stmtStatus->execute()){
            if($stmtDeleteCategory->execute()){
                return true;
            } else {
                return false;
            }
        }
    
        return false;
        
    }
}