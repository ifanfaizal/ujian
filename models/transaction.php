<?php
class Transaction{
 
    // database connection and table name
    private $conn;
    private $table_name = "transactions";
 
    // object properties
    public $id;
    public $cust_id;
    public $cat_id;
    public $weight;
    public $total_price;
    public $modified;
    public $pickup_point_lat;
    public $pickup_point_lng;
    public $destination_point_lng;
    public $destination_point_lat;
    public $mobile_phone;
    public $created_at;
    public $status;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read transactions
    public function read($statusCode, $idUser){
        $query = "SELECT
                    t.id, t.cust_id, t.cat_id, t.weight, t.total_price, ls.modified, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id AND ls.status = ". $statusCode ."
                    WHERE t.cust_id = " . $idUser . ";";

        if ($statusCode == 0) {
            $query = "SELECT
                    t.id, t.cust_id, t.cat_id, t.weight, t.total_price, ls.modified, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id AND (ls.status = ". $statusCode ." OR ls.status = 1 OR ls.status = 2)
                    WHERE t.cust_id = " . $idUser . ";";
        }

        // select all query
        
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    public function readAdmin($statusCode){
        $query = "SELECT
                    t.id, t.cust_id, t.cat_id, t.weight, t.total_price, ls.modified, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id AND ls.status = ". $statusCode .";";

        if ($statusCode == 0) {
            $query = "SELECT
                    t.id, t.cust_id, t.cat_id, t.weight, t.total_price, ls.modified, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id AND (ls.status = ". $statusCode ." OR ls.status = 1 OR ls.status = 2);";
        }

        // select all query
        
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // read transactions
    public function readAll($idUser){
        // select all query
        $query = "SELECT
                    t.id, t.cust_id, t.cat_id, c.category_name, t.weight, t.total_price, t.modified, t.created_at, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone, ls.status
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id
                    INNER JOIN
                        category c
                            ON t.cat_id = c.id
                    WHERE t.cust_id = " . $idUser . ";";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    public function readAllAdmin(){
        // select all query
        $query = "SELECT
                    t.id, t.cust_id, u.username, t.cat_id, c.category_name, t.weight, t.total_price, t.modified, t.created_at, t.pickup_point_lat, t.pickup_point_lng, t.destination_point_lng, t.destination_point_lat, t.mobile_phone, ls.status
                FROM
                    " . $this->table_name . " t
                    INNER JOIN
                        laundry_status ls
                            ON t.id = ls.transaction_id
                    INNER JOIN
                        category c
                            ON t.cat_id = c.id
                    INNER JOIN
                        users u
                            ON t.cust_id = u.id;";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // used when filling up the update transaction form
    function readOne(){
    
        // query to read single record
        $query = "SELECT
                    l.name as level_name, u.id, u.transactionname, u.password, u.level, u.created, u.email
                FROM
                    " . $this->table_name . " u
                    INNER JOIN
                        level_transactions l
                            ON u.level = l.id
                WHERE
                    u.transactionname = ?
                LIMIT
                    0,1;";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of transaction to be updated
        $stmt->bindParam(1, $this->transactionname);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (password_verify($this->password, $row['password'])) {
            // set values to object properties
            $this->id = $row['id'];
            $this->transactionname = $row['transactionname'];
            $this->password = $row['password'];
            $this->email = $row['email'];
            $this->level = $row['level'];
        } else {
            $this->transactionname = null;
        }
    }

    // update the product
    function changeStatus(){
    
        // update query
        $query = "UPDATE
                    laundry_status
                SET
                    status = :status
                WHERE
                    transaction_id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':status', $this->status);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // create transaction
    function create(){
    
        try {
            // query to insert record
            $query = "INSERT INTO
                    " . $this->table_name . "
                    SET
                        cust_id=:cust_id, cat_id=:cat_id, weight=:weight, total_price=:total_price, pickup_point_lat=:pickup_point_lat,
                        pickup_point_lng=:pickup_point_lng, destination_point_lat=:destination_point_lat, destination_point_lng=:destination_point_lng,
                        mobile_phone=:mobile_phone, created_at=:created_at;";

            $queryForStatus = "INSERT INTO laundry_status 
                                SET 
                                    transaction_id=:transaction_id, status=:status;";

            // prepare query
            $stmt = $this->conn->prepare($query);
            $stmtStatus = $this->conn->prepare($queryForStatus);

            // sanitize
            $this->cust_id=htmlspecialchars(strip_tags($this->cust_id));
            $this->cat_id=htmlspecialchars(strip_tags($this->cat_id));
            $this->weight=htmlspecialchars(strip_tags($this->weight));
            $this->total_price=htmlspecialchars(strip_tags($this->total_price));
            $this->pickup_point_lat=htmlspecialchars(strip_tags($this->pickup_point_lat));
            $this->pickup_point_lng=htmlspecialchars(strip_tags($this->pickup_point_lng));
            $this->destination_point_lat=htmlspecialchars(strip_tags($this->destination_point_lat));
            $this->destination_point_lng=htmlspecialchars(strip_tags($this->destination_point_lng));
            $this->mobile_phone=htmlspecialchars(strip_tags($this->mobile_phone));
            $this->created_at=htmlspecialchars(strip_tags($this->created_at));

            // bind values
            $stmt->bindParam(":cust_id", $this->cust_id);
            $stmt->bindParam(":cat_id", $this->cat_id);
            $stmt->bindParam(":weight", $this->weight);
            $stmt->bindParam(":total_price", $this->total_price);
            $stmt->bindParam(":pickup_point_lat", $this->pickup_point_lat);
            $stmt->bindParam(":pickup_point_lng", $this->pickup_point_lng);
            $stmt->bindParam(":destination_point_lat", $this->destination_point_lat);
            $stmt->bindParam(":destination_point_lng", $this->destination_point_lng);
            $stmt->bindParam(":mobile_phone", $this->mobile_phone);
            $stmt->bindParam(":created_at", $this->created_at);

            // execute query
            if($stmt->execute()){
                $lastId = $this->conn->lastInsertId();
                $statusInit = 0;
                $stmtStatus->bindParam(":transaction_id", $lastId);
                $stmtStatus->bindParam(":status", $statusInit);

                if($stmtStatus->execute()){
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}