<?php
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "users";
 
    // object properties
    public $id;
    public $username;
    public $password;
    public $email;
    public $level;
    public $created;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read users
    public function read(){
 
        // select all query
        $query = "SELECT
                    l.name as level_name, u.id, u.username, u.password, u.level, u.created, u.email
                FROM
                    " . $this->table_name . " u
                    INNER JOIN
                        level_users l
                            ON u.level = l.id
                ORDER BY
                    u.created DESC;";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // used when filling up the update user form
    function readOne(){
    
        // query to read single record
        $query = "SELECT
                    l.name as level_name, u.id, u.username, u.password, u.level, u.created, u.email
                FROM
                    " . $this->table_name . " u
                    INNER JOIN
                        level_users l
                            ON u.level = l.id
                WHERE
                    u.username = ?
                LIMIT
                    0,1;";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of user to be updated
        $stmt->bindParam(1, $this->username);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (password_verify($this->password, $row['password'])) {
            // set values to object properties
            $this->id = $row['id'];
            $this->username = $row['username'];
            $this->password = $row['password'];
            $this->email = $row['email'];
            $this->level = $row['level'];
        } else {
            $this->username = null;
        }
    }

    // create user
    function create(){
    
        try {
            // query to insert record
            $query = "INSERT INTO
            " . $this->table_name . "
            SET
                username=:username, password=:password, email=:email, level=:level, created=:created;";

            // prepare query
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->username=htmlspecialchars(strip_tags($this->username));
            $this->password=htmlspecialchars(strip_tags($this->password));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->level=htmlspecialchars(strip_tags($this->level));
            $this->created=htmlspecialchars(strip_tags($this->created));

            // bind values
            $stmt->bindParam(":username", $this->username);
            $stmt->bindParam(":password", $this->password);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":level", $this->level);
            $stmt->bindParam(":created", $this->created);

            // execute query
            if($stmt->execute()){
                return true;
            }

            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
        
    }
}