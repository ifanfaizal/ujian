<?php
class Price{
 
    // database connection and table name
    private $conn;
    private $table_name = "pricing";
 
    // object properties
    public $category_id;
    public $category_name;
    public $price;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read prices
    public function read(){
 
        // select all query
        $query = "SELECT
                    p.category_id, c.category_name, p.price
                FROM
                    " . $this->table_name . " p
                    INNER JOIN
                        category c
                            ON p.category_id = c.id;";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // used when filling up the update price form
    function readOne(){
    
        // query to read single record
        $query = "SELECT
                    l.name as level_name, u.id, u.pricename, u.password, u.level, u.created, u.email
                FROM
                    " . $this->table_name . " u
                    INNER JOIN
                        level_prices l
                            ON u.level = l.id
                WHERE
                    u.pricename = ?
                LIMIT
                    0,1;";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of price to be updated
        $stmt->bindParam(1, $this->pricename);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (password_verify($this->password, $row['password'])) {
            // set values to object properties
            $this->id = $row['id'];
            $this->pricename = $row['pricename'];
            $this->password = $row['password'];
            $this->email = $row['email'];
            $this->level = $row['level'];
        } else {
            $this->pricename = null;
        }
    }

    // create price
    function create(){
    
        try {
            // query to insert record
            $query = "INSERT INTO
            " . $this->table_name . "
            SET
                pricename=:pricename, password=:password, email=:email, level=:level, created=:created;";

            // prepare query
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->pricename=htmlspecialchars(strip_tags($this->pricename));
            $this->password=htmlspecialchars(strip_tags($this->password));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->level=htmlspecialchars(strip_tags($this->level));
            $this->created=htmlspecialchars(strip_tags($this->created));

            // bind values
            $stmt->bindParam(":pricename", $this->pricename);
            $stmt->bindParam(":password", $this->password);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":level", $this->level);
            $stmt->bindParam(":created", $this->created);

            // execute query
            if($stmt->execute()){
                return true;
            }

            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
        
    }
}