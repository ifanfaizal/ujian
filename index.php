<?php 
session_start(); 
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
}
?>
<!DOCTYPE html>
<html>
<title>Cuciin</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="./assets/css/index.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon" href="./assets/images/favicon.ico" type="image/ico">
<!--webfonts-->
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700|Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<!--//webfonts-->

<body id="myPage">

<!-- Navbar -->
<div class="w3-top">
    <div class="w3-bar w3-theme-d2 w3-left-align">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-hover-light-blue w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
        <a href="#" class="w3-bar-item w3-button w3-blue"><i class="fa fa-home w3-margin-right"></i>Beranda</a>
        <a href="#about" class="w3-bar-item w3-button w3-hide-small w3-hover-light-blue">Tentang</a>
        <a href="#team" class="w3-bar-item w3-button w3-hide-small w3-hover-light-blue">Tim</a>
        <a href="#contact" class="w3-bar-item w3-button w3-hide-small w3-hover-light-blue">Kontak Kami</a>
    
        <a onclick="document.getElementById('id01').style.display='block'" class="w3-bar-item w3-button w3-hide-small w3-hover-light-blue">
            <i class="fa fa-search w3-margin-right"></i>CARI LAUNDRY
        </a>
    
        <?php if (!isset($username)) echo('<button class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-light-blue"><a href="./views/auth.php#register">Daftar</a></button>'); ?>
        <?php echo(!isset($username) ? '<button class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-light-blue"><a href="./views/auth.php">Masuk</a></button>' : '<button class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-light-blue"><a href="./controllers/user/logout.php">Logout</a></button>'); ?>
        <?php if (isset($username)) echo('<button class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-light-blue"><a href="./views/user_page.php"><i class="fa fa-user w3-margin-right"></i>Dashbor</a></button>') ?>
    </div>
</div>

<!-- Image Header -->
<div class="w3-display-container w3-animate-opacity">
  <img src="./assets/images/home_image.jpg" alt="loundry" style="width:100%;min-height:350px;max-height:600px;">
  <div class="w3-display-bottomleft w3-padding w3-hide-small" style="width:35%">
      <div class="w3-black w3-opacity w3-hover-opacity-off w3-padding-large w3-round-large">
        <h1 class="w3-xlarge">CUCIIN</h1>
        <hr class="w3-opacity">
        <p>Versi Android: Lebih Mudah dan Fleksibel</p>
        <p><button class="w3-button w3-block w3-green w3-round" onclick="document.getElementById('download').style.display='block'">Unduh</button></p>
      </div>
    </div>
</div>

</div>
<!-- Modal -->
<div id="id01" class="w3-modal">
  <div class="w3-modal-content w3-card-4 w3-animate-top">
    <header class="w3-container w3-blue w3-display-container"> 
      <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-blue w3-display-topright"><i class="fa fa-remove"></i></span>
      <h4>CARI LAUNDRY</h4>
      <form>
        <div class="w3-row-padding" style="margin:0 -16px;">
          <div class="w3-half w3-margin-bottom">
            <label>Nama</label>
            <input class="w3-input w3-border" type="text" name="Nama" required>
          </div>
          <div class="w3-half">
            <label>Alamat</label>
            <input class="w3-input w3-border" type="text"  name="Alamat" required>
          </div>
        </div>
        <div class="w3-row-padding" style="margin:8px -16px;">
          <div class="w3-half w3-margin-bottom">
            <label>Jenis Laundry</label>
            <SELECT name="Jenis" class="w3-input">
              <option value="Satuan">Satuan</option>
              <option value="Kiloan">Kiloan</option>
            </SELECT>
          </div>
          <div class="w3-half">
            <label>Lama Laundry</label>
            <select class="w3-input" name="Lama">
                <option value="hari1">1 Hari</option>
                <option value="hari3">3 Hari</option>
            </select>
          </div>
        </div>
        <div class="w3-row-padding" style="margin:8px -16px;">
          <div class="w3-half w3-margin-bottom">
            <label>Metode Pembayaran</label>
            <select class="w3-input" name="Bayar">
                <option value="ditempat">Bayar di Tempat</option>
                <option value="transfer">Transfer</option>
            </select>
          </div>
        </div>
        <button class="w3-button w3-black" type="submit"><i class="fa fa-search w3-margin-right"></i>Cari</button>
      </form>
      </form>
    </header>
    <footer class="w3-container w3-blue">
        <p></p>
    </footer>
  </div>
</div>

<div id="download" class="w3-modal w3-animate-opacity">
  <div class="w3-modal-content" style="padding:32px">
    <div class="w3-container w3-white">
      <i onclick="document.getElementById('download').style.display='none'" class="fa fa-remove w3-xlarge w3-button w3-transparent w3-right w3-xlarge"></i>
      <h2 class="w3-wide">Unduh</h2>
      <p>Mengunduh Aplikasi Melalui Play Store</p>
      <p><input class="w3-input w3-border" type="text" placeholder="Masukkan E-mail"></p>
      <button type="button" class="w3-button w3-block w3-padding-large w3-blue w3-margin-bottom" onclick="document.getElementById('download').style.display='none'">Unduh</button>
    </div>
  </div>
</div>

<!-- Work Row -->
<div class="w3-row-padding w3-padding-64 w3-theme-l1" id="about">
    <h2>Tentang Kami</h2>
    <p>
        CUCIIN merupakan jasa laundry kiloan yang memiliki workshop <br>di Jl. TB Simatupang No.51B, RT.1/RW.8, Jati Padang, Ps. Minggu
        <br> Kami juga menerapkan jasa antar jemput laundry. So, tunggu apalagi . . .
    </p>
    <hr style="width: 43%">
    <h3 style="">Kami siap <b style="color: #6bdfff;">CUCIIN</b> pakaian kotor kamu sekarang juga!</h3>
</div>

<!-- Team Container -->
<div class="w3-container w3-padding-64 w3-center " id="team">
<h2>Tim Kami</h2>
<div class="w3-quarter" >
  <h3></h3>
  <p></p>
</div>
<div class="w3-quarter" >
  <img src="./assets/images/team2.jpg" alt="Diokta Redho Lastin" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Diokta Redho Lastin</h3>
  <p>CEO 1</p>
</div>
<div class="w3-quarter">
  <img src="./assets/images/team3.jpg" alt="Ifan Faizal Adnan" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Ifan Faizal Adnan</h3>
  <p>CEO 2</p>
</div>
</div>
</div>



<!-- Contact Container -->
<div class="w3-container w3-padding-64 w3-theme-l5" id="contact">
  <div class="w3-row">
    <div class="w3-col m5">
    <div class="w3-padding-16"><span class="w3-xlarge w3-border-light-blue w3-bottombar">Kontak Kami</span></div>
      <p><i class="fa fa-map-marker w3-text-light-blue w3-xlarge"></i>  Jl. TB Simatupang No.51B, RT.1/RW.8, Jati Padang, Ps. Minggu, <br>Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12520</p>
      <p><i class="fa fa-phone w3-text-light-blue w3-xlarge"></i>  +62 81284718812</p>
      <p><i class="fa fa-envelope w3-text-light-blue w3-xlarge"></i>  Cuciin@gmail.com</p>
    </div>
    <div class="w3-col m7">
      <form class="w3-container w3-card-4 w3-padding-16 w3-white" action="/action_page.php" target="_blank">
      <div class="w3-section">      
        <label>Nama</label>
        <input class="w3-input" type="text" name="Name" required>
      </div>
      <div class="w3-section">      
        <label>Email</label>
        <input class="w3-input" type="text" name="Email" required>
      </div>
      <div class="w3-section">      
        <label>Pesan</label>
        <input class="w3-input" type="text" name="Message" required>
      </div>  
      <button type="submit" class="w3-button w3-right w3-theme">Kirim</button>
      </form>
    </div>
  </div>
</div>

<!-- Image of location/map -->
<!-- <img src="/w3images/map.jpg" class="w3-image w3-greyscale-min" style="width:100%;"> -->

<!-- Footer -->
<footer class="w3-container w3-theme-d1 w3-center">
  <h4 style="margin-top: 50px;">Ikuti Kami</h4>
  <a href="#">
    <div class="social-icon">
        <i class="fab fa-facebook"></i>
    </div>
</a>
 <a href="#">
    <div class="social-icon">
        <i class="fab fa-twitter"></i>
    </div>
</a>
 <a href="#">
    <div class="social-icon">
        <i class="fab fa-codepen"></i>
    </div>
</a>
<a href="#">
    <div class="social-icon">
        <i class="fab fa-behance"></i>
    </div>
</a>
<a href="#">
    <div class="social-icon">
        <i class="fab fa-dribbble"></i>
    </div>
</a>
</div>
<p style="margin-top: 50px;">PT CUCIIN YUK &copy; 2018</p>

  <div style="position:relative;bottom:100px;z-index:1;" class="w3-tooltip w3-right">
    <span class="w3-text w3-padding w3-blue w3-hide-small">Go To Top</span>   
    <a class="w3-button w3-theme" href="#myPage"><span class="w3-xlarge">
    <i class="fa fa-chevron-circle-up"></i></span></a>
  </div>
</footer>


</body>
</html>
