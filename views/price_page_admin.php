<?php 
    session_start();
    if (!isset($_SESSION['username'])) {
        echo("<script>alert('Anda harus login dahulu');document.location.href='./auth.php'</script>");
    } else {
        if ($_SESSION['level'] != '1') {
            echo("<script>alert('Anda tidak punya akses ke halaman ini');document.location.href='./auth.php'</script>");
        }
    }
?>

<?php include('./header.php'); ?>
<div class="items">
    <h1 class="subhead">Daftar Kategori + Harga Cuciin</h1>
    <table id="price-table" class="pure-table pure-table-bordered">
        <thead>
            <tr>
                <th class="text-center">Kategori Cucian</th>
                <th class="text-center">Harga per Kilogram</th>
                <th class="text-center">Aksi</th>
            </tr>
        </thead>
    </table>
</div>
<br>
<h1 class="subhead">Tambah Kategori dan Harga</h1>
<div class="pure-g">
    <form class="pure-form">
        <fieldset>
            <input id="category_name" class="formVal" name="category_name" type="text" placeholder="Nama Kategori" maxlength="12" required>
            <input id="price" class="formVal" style="margin-left: 10px;" name="price" type="text" placeholder="Harga Kategori" maxlength="12" required>
            <button id="submitForm" type="button" style="margin-left: 10px;" class="pure-button pure-button-primary">Submit</button>
        </fieldset>
    </form>
</div>
<?php include('./footer.php'); ?>