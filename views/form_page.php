<?php 
    session_start();
    if (!isset($_SESSION['username'])) {
        echo("<script>alert('Anda harus login dahulu');document.location.href='./auth.php'</script>");
    } else {
        if ($_SESSION['level'] != '2') {
            echo("<script>alert('Anda tidak punya akses ke halaman ini');document.location.href='./auth.php'</script>");
        }
    }
?>

<?php include('./header.php'); ?>
<div class="pure-g">
    <div class="pure-u-1-2">
        <div class="items">
            <h1 class="subhead">Formulir Cuciin</h1>
            <form class="pure-form pure-form-stacked">
                <fieldset>
                    <div class="group-form">
                        <label for="hp">No. HP*</label>
                        <input id="hp" class="formVal" name="mobile_phone" type="text" placeholder="No. HP" maxlength="12" required>
                    </div>

                    <div class="group-form">
                        <label for="alamat_jemput">Alamat Penjemputan*</label>
                        <input id="alamat_jemput" type="text" readonly placeholder="Pilih di Map" required>
                        <input id="alamat_jemput_lat" class="formVal" name="pickup_point_lat" type="hidden" required>
                        <input id="alamat_jemput_lng" class="formVal" name="pickup_point_lng" type="hidden" required>
                    </div>

                    <div class="group-form">
                        <label for="alamat_antar">Alamat Pengantaran*</label>
                        <input id="alamat_antar" type="text" readonly placeholder="Pilih di Map" required>
                        <input id="alamat_antar_lat" class="formVal" name="destination_point_lat" type="hidden" required>
                        <input id="alamat_antar_lng" class="formVal" name="destination_point_lng" type="hidden" required>
                    </div>

                    <div class="group-form">
                        <label for="kategori">Pilih Kategori Cucian*</label>
                        <select id="kategori" onchange="getKategoriSelected(this);" name="category">
                            <option value="pilih">-- Pilih Kategori --</option>
                        </select>
                        <input id="cat_id" type="hidden" class="formVal" name="cat_id" required />
                        <input id="cat_price" type="hidden" class="formVal" name="cat_price" required />
                    </div>

                    <div class="group-form">
                        <label for="berat">Berat *</label>
                        <div class="pure-u-1" style="width: 500px;">
                            <input id="berat" class="formVal" name="weight" min="1" value="1" style="width: 15%; display: inline-block;" type="number" placeholder="Berat" maxlength="2" required>
                            <span style="width: 20%; margin-left: 10px;">Kg</span>
                        </div>
                    </div>

                    <div class="group-form">
                        <button id="submitForm" type="button" class="pure-button pure-button-primary">Submit</button>
                    </div>

                    <span class="pure-form-message">*Kolom ini wajib diisi</span>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="pure-u-1-2">
        <div class="items">
            <h1 class="subhead text-white">Formulir Cuciin</h1>
            <!--map div-->
            <div id="mapid"></div>
        </div>
    </div>
</div>
<?php include('./footer.php'); ?>
<script type="text/javascript">
    var username = '<?php echo($_SESSION['username']); ?>'
</script>
<script type="text/javascript" src="../assets/js/map.js"></script>