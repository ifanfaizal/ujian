<?php 
    session_start();
    if (!isset($_SESSION['username'])) {
        echo("<script>alert('Anda harus login dahulu');document.location.href='./auth.php'</script>");
    } else {
        if ($_SESSION['level'] != '2') {
            echo("<script>alert('Anda tidak punya akses ke halaman ini');document.location.href='./auth.php'</script>");
        }
    }
?>

<?php include('./header.php'); ?>
<div class="items">
    <h1 class="subhead">Daftar Harga Cuciin</h1>
    <table id="price-table" class="pure-table pure-table-bordered">
        <thead>
            <tr>
                <th class="text-center">Kategori Cucian</th>
                <th class="text-center">Harga per Kilogram</th>
            </tr>
        </thead>
    </table>
</div>
<?php include('./footer.php'); ?>