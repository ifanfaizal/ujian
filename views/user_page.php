<?php 
    session_start();
    if (!isset($_SESSION['username'])) {
        echo("<script>alert('Anda harus login dahulu');document.location.href='./auth.php'</script>");
    } else {
        if ($_SESSION['level'] != '2') {
            echo("<script>alert('Anda tidak punya akses ke halaman ini');document.location.href='./auth.php'</script>");
        }
    }
?>

<?php include('./header.php'); ?>
<div class="items">
    <h1 class="subhead">Dasbor</h1>
</div>

<div class="pure-g">

    <div class="pure-u-1 pure-u-md-1-2">
        <div class="column-block">
            <div class="column-block-header column-success">
                <h2>Jumlah Cucian Aktif</h2>
                <span id="active-month-transaction" class="column-block-info">- <span>bulan ini</span></span>
            </div>
            <ul class="column-block-list">
                <li>Hari Ini <span id="active-today-transaction" class="buble-success button-small pull-right">-</span></li>
                <li>Total <span id="active-total-transaction" class="buble-warning button-small pull-right">-</span></li>
            </ul>
        </div>
    </div>

    <div class="pure-u-1 pure-u-md-1-2">
        <div class="column-block">
            <div class="column-block-header column-warning">
                <h2>Jumlah Cucian Selesai</h2>
                <span id="finish-month-transaction" class="column-block-info">- <span>bulan ini</span></span>
            </div>
            <ul class="column-block-list">
                <li>Hari Ini <span id="finish-today-transaction" class="buble-success button-small pull-right">100</span></li>
                <li>Total <span id="finish-total-transaction" class="buble-warning button-small pull-right">10000</span></li>
            </ul>
        </div>
    </div>

    <!-- <div class="pure-u-1 pure-u-md-1-3">
        <div class="column-block">
            <div class="column-block-header">
                <h2>Options</h2>
                <span class="column-block-info">1000 <span>this month</span></span>
            </div>
            <ul class="column-block-list">
                <li>Today <span class="buble-success button-small pull-right">100</span></li>
                <li>Yesterday <span class="buble-secondary button-small pull-right">1000</span></li>
                <li>Total <span class="buble-warning button-small pull-right">10000</span></li>
            </ul>
        </div>
    </div> -->

</div>

<div class="pure-g">
    <div class="pure-u-1">
        <div class="column-block">
            <table id="cucian-table" class="pure-table pure-table-horizontal">
                <thead>
                    <tr>
                        <th class="text-center">Kategori Cucian</th>
                        <th class="text-center">Berat Cucian</th>
                        <th class="text-center">Total Harga</th>
                        <th class="text-center">No. HP</th>
                        <th class="text-center">Tgl. Masuk Cucian</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<?php include('./footer.php'); ?>