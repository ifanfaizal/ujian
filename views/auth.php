<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - Cuciin</title>
    <link href="https://fonts.googleapis.com/css?family=Eczar" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/alert.css">
    <link rel="stylesheet" href="../assets/css/auth.css">
</head>
<body>
<form id="form_login_regis" action="logging.php" method="POST">
    <div class="login-wrap">
	    <div class="login-html">
            <a href="../index.php">
                <h1 class="text-center title-white">CUCIIN</h1>
            </a>
            <div class="alert success">
                <input type="checkbox" id="alert2"/>
                <label class="close" title="close" for="alert2">
            <i class="icon-remove"></i>
            </label>
                <p id="success-text" class="inner"></p>
            </div>
            <div class="alert error">
                <input type="checkbox" id="alert1"/>
                <label class="close" title="close" for="alert1">
                <i class="icon-remove"></i>
                </label>
                <p id="error-text" class="inner"></p>
            </div>
            <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
            <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
            <div class="login-form">
                <div class="sign-in-htm">
                    <div class="group">
                        <label for="user_login" class="label">Username</label>
                        <input id="user_login" name="user_login" type="text" class="input formVal">
                    </div>
                    <div class="group">
                        <label for="pass_login" class="label">Password</label>
                        <input id="pass_login" name="pass_login" type="password" class="input formVal" data-type="password">
                    </div>
                    <div class="group">
                        <input id="check" type="checkbox" class="check" checked>
                        <label for="check"><span class="icon"></span> Keep me Signed in</label>
                    </div>
                    <div class="group">
                        <input type="button" id="btn_login" onclick="login()" class="button" value="Sign In">
                    </div>
                    <div class="hr"></div>
                    <div class="foot-lnk">
                        <a href="#forgot">Forgot Password?</a>
                    </div>
                </div>
                <div class="sign-up-htm">
                    <div class="group">
                        <label for="email_regis" class="label">Email Address</label>
                        <input id="email_regis" name="email_regis" type="text" class="input formValRegis">
                    </div>
                    <div class="group">
                        <label for="user_regis" class="label">Username</label>
                        <input id="user_regis" name="user_regis" type="text" class="input formValRegis">
                    </div>
                    <div class="group">
                        <label for="pass_regis" class="label">Password</label>
                        <input id="pass_regis" name="pass_regis" type="password" class="input formValRegis" data-type="password">
                    </div>
                    <div class="group">
                        <label for="pass_conf_regis" class="label">Repeat Password</label>
                        <input id="pass_conf_regis" name="pass_conf_regis" type="password" class="input" data-type="password">
                    </div>
                    <div class="group">
                        <input type="button" id="btn_register" onclick="register()" class="button" value="Sign Up">
                    </div>
                    <div class="hr"></div>
                    <div class="foot-lnk">
                        <label for="tab-1">Already Member?</a>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</form>
<?php include '../assets/html/loading.html';?>
<script src="../assets/js/auth.js"></script>
</body>
</html>