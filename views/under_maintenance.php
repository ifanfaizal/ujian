<?php 
    session_start();
    if (!isset($_SESSION['username'])) {
        echo("<script>alert('Anda harus login dahulu');document.location.href='./auth.php'</script>");
    } 
?>

<?php include('./header.php'); ?>
<style>
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1>Coming soon!</h1>
    <img src="../assets/images/sorry.png" />
    <div>
        <p>Fitur ini belum tersedia. Silakan kunjungi di lain waktu ya!</p>
        <p>&mdash; CUCIIN TEAM</p>
    </div>
</article>
<?php include('./footer.php'); ?>