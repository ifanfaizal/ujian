<div class="sidebar pure-u-1 pure-u-md-3-24">
    <div id="menu">
        <div class="pure-menu">
            <div class="text-center" style="margin-top: 16px;">
                <img src="../assets/images/circle_avatar.png" width="95px" height="95px" />
            </div>
            <p class="pure-menu-heading text-center" style="margin-top: 0;">
                Halo, <br> <?php echo($_SESSION['username']) ?> <br><br>
                <a href="../controllers/user/logout.php" class="pure-button button-xsmall button-error text-white">LOGOUT &raquo;</a>
            </p>
            <ul class="pure-menu-list">
                <li id="menu-1">
                    <a href="<?php echo($_SESSION['level']) == '1' ? './admin_page.php' : './user_page.php' ?>" class="pure-menu-link">Dasbor</a>
                </li>
                <li id="menu-2">
                    <a href="<?php echo($_SESSION['level']) == '1' ? './price_page_admin.php' : './price_page.php' ?>" class="pure-menu-link">
                        <?php echo($_SESSION['level']) == '1' ? 'Kategori & Harga' : 'Daftar Harga' ?>
                    </a>
                </li>
                <?php 
                    echo($_SESSION['level']) == '1' ? '' : '<li id="menu-3">
                        <a href="./form_page.php" class="pure-menu-link">Form Cuciin</a>
                    </li>'
                ?>
                <li id="menu-4">
                    <a href="./under_maintenance.php" class="pure-menu-link">Promo</a>
                </li>
                <!-- <li id="menu-5">
                    <a href="profile.html" class="pure-menu-link">Profil</a>
                </li> -->
            </ul>
        </div>
    </div>
</div>