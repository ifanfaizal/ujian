<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->user_regis) &&
    !empty($data->pass_regis) &&
    !empty($data->email_regis)
) {
    // set user property values
    $user->username = $data->user_regis;
    $user->password = password_hash($data->pass_regis, PASSWORD_DEFAULT);
    $user->email = $data->email_regis;
    $user->level = '2'; // Customer level
    $user->created = date('Y-m-d H:i:s');
 
    // create the user
    if($user->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Anda berhasil registrasi. <a href='./auth.php'><u>Klik di sini</u></a> untuk login."));
    }
    // if unable to create the user, tell the user
    else{
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create user."));
    }
} else {
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to get user. Data is incomplete."));
    return;
}