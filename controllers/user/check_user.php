<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->user_login) &&
    !empty($data->pass_login)
) {
    $user->username = $data->user_login;
    $user->password = $data->pass_login;
} else {
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to get user. Data is incomplete."));
    return;
}
 
// read the details of user to be edited
$user->readOne();
 
if($user->username!=null){
    // create array
    $user_arr = array(
        "id" =>  $user->id,
        "username" => $user->username,
        "email" => $user->email,
        "password" => $user->password,
        "level" => $user->level,
    );

    // set session
    session_start();
    $_SESSION['id'] = $user->id;
    $_SESSION['username'] = $user->username;
    $_SESSION['level'] = $user->level;
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($user_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user user does not exist
    echo json_encode(array("message" => "Username / password salah!"));
}
?>