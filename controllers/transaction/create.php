<?php
session_start();

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/transaction.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare transaction object
$transaction = new Transaction($db);

$idUser = isset($_SESSION['id']) ? $_SESSION['id'] : die();

// get posted data
$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->mobile_phone) &&
    !empty($data->pickup_point_lat) &&
    !empty($data->pickup_point_lng) &&
    !empty($data->destination_point_lat) &&
    !empty($data->destination_point_lng) &&
    !empty($data->cat_id) &&
    !empty($data->cat_price) &&
    !empty($data->weight)
) {
    // set transaction property values
    $transaction->cust_id = $idUser;
    $transaction->cat_id = $data->cat_id;
    $transaction->weight = $data->weight;
    $transaction->mobile_phone = $data->mobile_phone;
    $transaction->total_price = $data->weight * $data->cat_price;
    $transaction->pickup_point_lat = $data->pickup_point_lat;
    $transaction->pickup_point_lng = $data->pickup_point_lng;
    $transaction->destination_point_lat = $data->destination_point_lat;
    $transaction->destination_point_lng = $data->destination_point_lng;
    $transaction->created_at = date('Y-m-d H:i:s', strtotime("+7 hours"));
 
    // create the transaction
    if($transaction->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the transaction
        echo json_encode(array("message" => "Anda berhasil melakukan pemesanan CUCIIN. Silakan lakukan pembayaran."));
    }
    // if unable to create the transaction, tell the transaction
    else{
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the transaction
        echo json_encode(array("message" => "Unable to create transaction."));
    }
} else {
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the transaction
    echo json_encode(array("message" => "Unable to create transaction. Data is incomplete."));
    return;
}