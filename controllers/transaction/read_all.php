<?php
session_start();
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/transaction.php';
 
// instantiate database and transaction object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$transaction = new Transaction($db);

$idUser = isset($_SESSION['id']) ? $_SESSION['id'] : die();

// query transactions
$stmt = $transaction->readAll($idUser);
$num = $stmt->rowCount();
$transactions_arr=array();

// check if more than 0 record found
if($num>0){

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $status_ket = "";

        if ($status == 0) {
            $status_ket = "Dalam Penjemputan";
        } elseif ($status == 1) {
            $status_ket = "Dalam Pencucian";
        } elseif ($status == 2) {
            $status_ket = "Dalam Pengantaran";
        } else {
            $status_ket = "Cucian Selesai";
        }

        $transaction_item=array(
            "id" => $id,
            "cust_id" => $cust_id,
            "cat_id" => $cat_id,
            "cat_name" => $category_name,
            "weight" => $weight,
            "total_price" => $total_price,
            "modified" => $modified,
            "created_at" => $created_at,
            "pickup_point_lat" => $pickup_point_lat,
            "pickup_point_lng" => $pickup_point_lng,
            "destination_point_lng" => $destination_point_lng,
            "destination_point_lat" => $destination_point_lat,
            "mobile_phone" => $mobile_phone,
            "status_ket" => $status_ket,
            "status" => $status
        );

        array_push($transactions_arr, $transaction_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show transactions data in json format
    echo json_encode($transactions_arr);
} else{
 
    // set response code - 200 OK
    http_response_code(200);
 
    echo json_encode($transactions_arr);
}