<?php
session_start();
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/transaction.php';
 
// instantiate database and transaction object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$transaction = new Transaction($db);

// set ID property of record to read
$status = isset($_GET['status']) ? $_GET['status'] : die();
$idUser = isset($_SESSION['id']) ? $_SESSION['id'] : die();
 
// query transactions
$stmt = $transaction->read($status, $idUser);
$num = $stmt->rowCount();
// transactions array
$counterToday = 0;
$counterMonth = 0;
$counterTotal = 0;

// check if more than 0 record found
if($num>0){
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        // $transaction_item=array(
        //     "id" => $id,
        //     "cust_id" => $cust_id,
        //     "cat_id" => $cat_id,
        //     "weight" => $weight,
        //     "total_price" => $total_price,
        //     "modified" => $modified,
        //     "pickup_point_lat" => $pickup_point_lat,
        //     "pickup_point_lng" => $pickup_point_lng,
        //     "destination_point_lng" => $destination_point_lng,
        //     "destination_point_lat" => $destination_point_lat,
        //     "mobile_phone" => $mobile_phone
        // );
        
        if (date('d', strtotime($modified)) == date('d')) {
            $counterToday++;
        } 
        
        if (date('m', strtotime($modified)) == date('m')) {
            $counterMonth++;
        }

        $counterTotal++;
    }

    $transaction_item=array(
        "todayCount" => $counterToday,
        "monthCount" => $counterMonth,
        "totalCount" => $counterTotal,
    );      
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show transactions data in json format
    echo json_encode($transaction_item);
} else{
 
    // set response code - 200 OK
    http_response_code(200);

    $transaction_item=array(
        "todayCount" => $counterToday,
        "monthCount" => $counterMonth,
        "totalCount" => $counterTotal,
    );   
 
    echo json_encode($transaction_item);
}