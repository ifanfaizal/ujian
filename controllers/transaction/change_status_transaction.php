<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/transaction.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare transaction object
$transaction = new Transaction($db);
 
$id = isset($_GET['id']) ? $_GET['id'] : die();
$status = isset($_GET['status']) ? $_GET['status'] : die();
 
// set transaction property values
$transaction->id = $id;
$transaction->status = $status;
 
// update the transaction
if($transaction->changeStatus()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "Transaction was updated."));
}
// if unable to update the transaction, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update transaction."));
}
?>