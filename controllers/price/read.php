<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/price.php';
 
// instantiate database and price object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$price = new Price($db);
 
// query prices
$stmt = $price->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // prices array
    $prices_arr=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $price_item=array(
            "category_id" => $category_id,
            "category_name" => $category_name,
            "price" => $price,
        );
 
        array_push($prices_arr, $price_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show prices data in json format
    echo json_encode($prices_arr);
} else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the price no prices found
    echo json_encode(
        array("message" => "No prices found.")
    );
}