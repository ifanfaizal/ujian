<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../../config/database.php';
include_once '../../models/category.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare category object
$category = new Category($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->category_name) &&
    !empty($data->price)
) {
    // set category property values
    $category->category_name = $data->category_name;
    $category->price = $data->price;
 
    // create the category
    if($category->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the category
        echo json_encode(array("message" => "Anda berhasil membuat kategori baru."));
    }
    // if unable to create the category, tell the category
    else{
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the category
        echo json_encode(array("message" => "Unable to create category."));
    }
} else {
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the category
    echo json_encode(array("message" => "Unable to create category. Data is incomplete."));
    return;
}